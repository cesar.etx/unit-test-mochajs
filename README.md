# Unit-test-MochaJS

Run `npm install` to build the project. Install dependencies

Run `npm run test` to run test.


## Author

* **Dodany Girón**      - dodanygiron@gmail.com 
Análisis y DIseño de sistemas 2
USAC

## Tarea  2
//BEGIN
Entrega para el día Miércoles 23/09/2020
1. Clonar el repo 
2. Inicializar su repo  (opcional) npm install 
3. Verificar los test (opcional) npm run Teset 
4. Crear Rama feature/t2_Carné --> (ejemplo)   /feature/t2_2020111111

5. Crear sobre la raiz una clase con su carné.js (al mismo nivel de app.js)
    5.1 Crear 5 funciones. (Sean creativos)
    5.2 Crear una claseTest con su carnéTest.js (al mismo nivel de appTest.js)
    5.3 Crear una Prueba unitaria por función. (si desean más, son libres)

6. Verificación (opcional) : npm run test 

7. Subir los cambios
    - git add , git commit -m "Tarea-2 _nombre y _Carné" 
    - git push

8. Realizar Merge Request ( no Merge con la master)

9. Éxitos a todos (Recuerden no salir de casa)
//END